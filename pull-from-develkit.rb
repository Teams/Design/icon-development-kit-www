#!/usr/bin/env ruby

require 'yaml'
require "rexml/document"
require "fileutils"
include REXML

DEVELKIT = '../icon-development-kit'
SRC = "#{DEVELKIT}/src/icons.svg"
GFXYAML = '_data/icons.yaml'
gfxyaml = []

#INKSCAPE = 'inkscape'
PREFIX = "img/symbolic"
# SVGO is a Node.js SVG optimization tool install with 'sudo npm install -g svgo'
# script will skip if SVGO is not present
#SVGO = '/usr/bin/svgo'

def chopSVG(icon)
	FileUtils.mkdir_p(icon[:dir]) unless File.exists?(icon[:dir])
	unless (File.exists?(icon[:file]) && !icon[:forcerender])
		FileUtils.cp(SRC,icon[:file]) 
		puts " >> #{icon[:name]}"
		# extract the icon
		cmd = "#{INKSCAPE} -g #{icon[:file]} --select #{icon[:id]} --verb=\"FitCanvasToSelection;EditInvertInAllLayers"
		cmd += ";EditDelete;EditSelectAll;SelectionUnGroup;SelectionUnGroup;SelectionUnGroup;StrokeToPath;FileVacuum"
		cmd += ";FileSave;FileQuit;\" > /dev/null 2>&1" 
		system(cmd)
		# remove bounding rectangle
		#bounding rectangle is now a path. needs to be removed
		svgcrop = Document.new(File.new(icon[:file], 'r'))
		svgcrop.root.each_element("//path") do |path|
	    #puts(path.attributes['style'])
	    if path.attributes['style'].include? 'fill:none;'
		    #puts "DEBUG: found rect to remove #{path}"
		    path.remove
	    end
    end
		icon_f = File.new(icon[:file],'w+')
		icon_f.puts svgcrop
		icon_f.close
		# remove as many extraneous elements as possible with SVGO
		cmd = "#{SVGO} --pretty --disable=convertShapeToPath --disable=convertPathData --enable=removeStyleElement -i #{icon[:file]} -o  #{icon[:file]} > /dev/null 2>&1"
		system(cmd)
	else
		puts " -- #{icon[:name]} already exists"
	end
end #end of function

def get_output_filename(d,n)
	if (/rtl$/.match(n))
		outfile = "#{d}/#{n.chomp('-rtl')}-symbolic-rtl.svg"
	else
		outfile = "#{d}/#{n}-symbolic.svg"
	end
	return outfile
end


def get_output_filename(d,n)
	#if (/rtl$/.match(n))
	#	outfile = "#{d}/#{n.chomp('-rtl')}-symbolic-rtl.svg"
	#else
		outfile = "#{d}/#{n}-symbolic.svg"
	#end
	return outfile
end



#main
# Open SVG file.
svg = Document.new(File.new(SRC, 'r'))

puts "Looking for metadata in #{SRC}"
# Go through every layer.
svg.root.each_element("/svg/g[@inkscape:groupmode='layer']") do |context| 
	context_name = context.attributes.get_attribute("inkscape:label").value
	if context_name.end_with?("legacy")
		puts "Skipping layer '" + context_name + "'"
	else
		puts "Going through layer '" + context_name + "'"
		context.each_element("g") do |icon|
                  if !icon.elements["title"].nil?
                      icon_name = icon.elements["title"].text
		      tags = icon.attributes['inkscape:label'].split(" ")
		      #puts context_name, icon_name, tags
		      gfxyaml << ({ "filename" => icon_name, "context" => context_name, "tags" => tags  }) unless icon_name==''
                  end
		end
		File.open(GFXYAML, "w") {|f| f.write(gfxyaml.to_yaml) }
	end
end

#inkscape is always broken
if (false) #no rendering. use symbolic preview
	puts "Rendering from icons in #{SRC}"
	# Go through every layer.
	svg.root.each_element("/svg/g[@inkscape:groupmode='layer']") do |context| 
		context_name = context.attributes.get_attribute("inkscape:label").value
		if context_name.end_with?("legacy")
			puts "Skipping layer '" + context_name + "'"
		else
			puts "Going through layer '" + context_name + "'"
			context.each_element("g") do |icon|
				#puts "DEBUG #{icon.attributes.get_attribute('id')}"
				dir = "#{PREFIX}/#{context_name}"
                                if !icon.elements["title"].nil?
				  icon_name = icon.elements["title"].text
				  chopSVG({ :name => icon_name,
						:id => icon.attributes.get_attribute("id"),
						:dir => dir,
						:file => get_output_filename(dir, icon_name)})
                                end
			end
		end
	end
	puts "\nrendered all SVGs"
else #only render the icons passed
	icons = ARGV
	ARGV.each do |icon_name|
		icon = svg.root.elements["//g/title[text() = '#{icon_name}']"].parent
		dir = "#{PREFIX}/#{icon.parent.attributes['inkscape:label']}"
		chopSVG({	:name => icon_name,
					:id => icon.attributes["id"],
					:dir => dir,
					:file => get_output_filename(dir, icon_name),
					:forcerender => true})
	end
	puts "\nrendered #{ARGV.length} icons"
end
