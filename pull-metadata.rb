#!/usr/bin/env ruby

require 'yaml'
require "rexml/document"
require "fileutils"
include REXML

DEVELKIT = '../icon-development-kit'
SRC = "#{DEVELKIT}/src/icons.svg"
GFXYAML = '_data/icons.yaml'
gfxyaml = []

#INKSCAPE = 'inkscape'
PREFIX = "img/symbolic"
# SVGO is a Node.js SVG optimization tool install with 'sudo npm install -g svgo'
# script will skip if SVGO is not present
#SVGO = '/usr/bin/svgo'

#main
# Open SVG file.
svg = Document.new(File.new(SRC, 'r'))

puts "Looking for metadata in #{SRC}"
# Go through every layer.
svg.root.each_element("/svg/g[@inkscape:groupmode='layer']") do |context| 
	context_name = context.attributes.get_attribute("inkscape:label").value
	if context_name.end_with?("legacy")
		puts "Skipping layer '" + context_name + "'"
	else
		puts "Going through layer '" + context_name + "'"
		context.each_element("g") do |icon|
                  if !icon.elements["title"].nil?
                      icon_name = icon.elements["title"].text
		      tags = icon.attributes['inkscape:label'].split(" ")
		      #puts context_name, icon_name, tags
		      gfxyaml << ({ "filename" => icon_name, "context" => context_name, "tags" => tags  }) unless icon_name==''
                  end
		end
		File.open(GFXYAML, "w") {|f| f.write(gfxyaml.to_yaml) }
	end
end
